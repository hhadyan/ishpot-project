<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CRUDModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    public function __construct() {
        if (!session()->has('login_state')) {
            Redirect::to('login')->send();
        }

        $this->model    = new CRUDModel();
    }

    public function index()
    {
        if (session('role') != '01' && session('role') != '02') {
            Redirect::to('wp-admin')->send();
        }

        return view('admin/admin/data');
    }

    public function input()
    {
        if (session('role') != '01' && session('role') != '02') {
            Redirect::to('wp-admin')->send();
        }

        return view('admin/admin/input');
    }

    public function edit($id)
    {
        $data       = DB::table('admin')
                        ->where('admin.id', $id)
                        ->first();

        return view('admin/admin/edit', compact('data'));
    }

    public function ambilData(Request $req)
    {
        if (session('role') != '01' && session('role') != '02') {
            Redirect::to('wp-admin')->send();
        }

        $role   = null;
        if ($req->role != 'NULL') {
            $role   = $req->role;
        }

        $status = null;
        if ($req->status != 'NULL') {
            $status = $req->status;
        }

        $data       = DB::table('admin')
                        ->when($role, function ($query) use ($role) {
                            $query->where('role', $role);
                        })
                        ->when($status, function ($query) use ($status) {
                            $query->where('status', $status);
                        })
                        ->get();

        return $data;
    }

    public function save(Request $req)
    {
        if (session('role') != '01' && session('role') != '02') {
            Redirect::to('wp-admin')->send();
        }

        $data   = array(
            'nama'          => $req->nama,
            'username'      => $req->user,
            'password'      => Hash::make($req->pass),
            'no_hp'         => $req->nohp,
            'role'          => $req->role,
            'status'        => '01',
            'created_user'  => session('username'),
            'created_at'    => \Carbon\Carbon::now()
        );

        $result = $this->model->simpanData('admin', $data);

        return $result;
    }

    public function update(Request $req)
    {
        $id         = $req->id;
        $pass       = $req->passlama;

        if ($req->pass != 'undefined' || $req->pass != '') {
            $pass   = Hash::make($req->pass);
        }

        $data   = array(
            'nama'          => $req->nama,
            'username'      => $req->user,
            'password'      => $pass,
            'no_hp'         => $req->nohp,
            'role'          => $req->role,
            'updated_user'  => session('username'),
            'updated_at'    => \Carbon\Carbon::now()
        );

        $result = $this->model->updateData('admin', $data, $id);

        return $result;
    }

    public function delete(Request $req)
    {
        if (session('role') != '01' && session('role') != '02') {
            Redirect::to('wp-admin')->send();
        }

        $id     = $req->id;

        $result = $this->model->hapusData('admin', $id);

        return $result;
    }
}
