<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CRUDModel;

class APIController extends Controller
{
    public function __construct() {
        $this->model    = new CRUDModel();
    }

    public function publish(Request $req)
    {
        $tabel  = $req->tabel;
        $id     = $req->id;

        $result = $this->model->publish($tabel, $id);

        return $result;
    }

    public function publishAll(Request $req)
    {
        $data       = $req->json()->all();

        $dataTabel  = $data['datatbl'];
        $tabel      = $data['tabel'];

        foreach ($dataTabel as $value) {
            $result = $this->model->publish($tabel, $value['id']);
        }

        return $result;
    }

    public function draft(Request $req)
    {
        $tabel  = $req->tabel;
        $id     = $req->id;

        $result = $this->model->draft($tabel, $id);

        return $result;
    }

    public function draftAll(Request $req)
    {
        $data       = $req->json()->all();

        $dataTabel  = $data['datatbl'];
        $tabel      = $data['tabel'];

        foreach ($dataTabel as $value) {
            $result = $this->model->draft($tabel, $value['id']);
        }

        return $result;
    }

    public function delete(Request $req)
    {
        $tabel  = $req->tabel;
        $id     = $req->id;

        $result = $this->model->hapusData($tabel, $id);

        return $result;
    }
}
