<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class AuthController extends Controller
{
    public function index()
    {
        if (session('login_state') == '01') {
            Redirect::to('wp-admin')->send();
        }

        return view('admin/login');
    }

    public function login(Request $req)
    {
        $user   = $req->user;
        $pass   = $req->pass;

        try {
            $cek    = DB::table('admin')
                        ->where('username', $user)
                        ->where('status', '01')
                        ->first();

            $dump   = array(
                'hash_cek'      => Hash::check($pass, $cek->password),
                'pass_input'    => $pass,
                'pass_hash'     => $cek->password
            );

            if ($cek) {
                if (Hash::check($pass, $cek->password)) {
                    $data   = array(
                        'id'            => $cek->id,
                        'username'      => $cek->username,
                        'nama'          => $cek->nama,
                        'nohp'          => $cek->no_hp,
                        'role'          => $cek->role,
                        'login_state'   => '01'
                    );

                    session()->put($data);

                    $return   = array(
                        'msg'           => 'success'
                    );

                    return $return;
                } else {
                    $return   = array(
                        'msg'           => 'error',
                        'msg_data'      => 'Username atau Password yang Anda Masukkan Salah!'
                    );

                    return $return;
                }
            } else {
                $return   = array(
                    'msg'           => 'error',
                    'msg_data'      => 'Username atau Password yang Anda Masukkan Salah!'
                );

                return $return;
            }
        } catch (\Throwable $e) {
            $return   = array(
                'msg'           => 'error',
                'msg_data'      => $e->getMessage()
            );

            return $return;
        }
    }

    public function logout()
    {
        session()->flush();

        return redirect('/login');
    }
}
