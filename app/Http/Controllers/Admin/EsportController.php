<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CRUDModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class EsportController extends Controller
{
    public function __construct() {
        if (!session()->has('login_state')) {
            Redirect::to('login')->send();
        }

        $this->model    = new CRUDModel();
    }

    public function index()
    {
        $kategori   = $this->model->ambilWhereData('kategori', 'menu = "Esport"');

        return view('admin/esport/data', compact('kategori'));
    }

    public function input()
    {
        $kategori   = $this->model->ambilWhereData('kategori', 'menu = "Esport"');

        return view('admin/esport/input', compact('kategori'));
    }

    public function edit($id)
    {
        $kategori   = $this->model->ambilWhereData('kategori', 'menu = "Esport"');
        $data       = DB::table('esport')
                        ->select(
                            'esport.*',
                            'kategori.kategori AS kategori'
                        )
                        ->join('kategori', 'esport.kategoriId', 'kategori.id')
                        ->where('esport.id', $id)
                        ->first();

        return view('admin/esport/edit', compact('kategori', 'data'));
    }

    public function ambilData(Request $req)
    {
        $tipe       = null;
        if ($req->tipe != 'NULL') {
            $tipe       = $req->tipe;
        }

        $kategori   = null;
        if ($req->kategori != 'NULL') {
            $kategori   = $req->kategori;
        }

        $data       = DB::table('esport')
                        ->select(
                            'esport.*',
                            'kategori.kategori AS kategori'
                        )
                        ->when($tipe, function ($query) use ($tipe) {
                            $query->where('status', $tipe);
                        })
                        ->when($kategori, function ($query) use ($kategori) {
                            $query->where('kategoriId', $kategori);
                        })
                        ->join('kategori', 'esport.kategoriId', 'kategori.id')
                        ->get();

        return $data;
    }

    public function publish(Request $req)
    {
        if ($req->file('foto')) {
            $nama       = preg_replace('/\s+/', '', $req->nama);

            $namaFoto   = $nama . '.' . $req->file('foto')->getClientOriginalExtension();

            $req->file('foto')->move('assets/upload/esport', $namaFoto);
        }

        $data   = array(
            'nama'              => $req->nama,
            'harga'             => $req->harga,
            'deskripsi'         => $req->deskripsi,
            'tanggal_ev_mulai'  => date('Y-m-d', strtotime($req->mulai_event)),
            'tanggal_ev_akhir'  => date('Y-m-d', strtotime($req->akhir_event)),
            'tanggal_dft_mulai' => date('Y-m-d', strtotime($req->mulai_daftar)),
            'tanggal_dft_akhir' => date('Y-m-d', strtotime($req->akhir_daftar)),
            'slot_tersedia'     => $req->slot,
            'slot_tersisa'      => $req->slot,
            'kategoriId'        => $req->kategori,
            'foto'              => $namaFoto,
            'status'            => '01',
            'created_user'      => session('username'),
            'created_at'        => \Carbon\Carbon::now()
        );

        $result = $this->model->simpanData('esport', $data);

        return $result;
    }

    public function draft(Request $req)
    {
        if ($req->file('foto')) {
            $nama       = preg_replace('/\s+/', '', $req->nama);

            $namaFoto   = $nama . '.' . $req->file('foto')->getClientOriginalExtension();

            $req->file('foto')->move('assets/upload/esport', $namaFoto);
        }

        $data   = array(
            'nama'              => $req->nama,
            'harga'             => $req->harga,
            'deskripsi'         => $req->deskripsi,
            'tanggal_ev_mulai'  => date('Y-m-d', strtotime($req->mulai_event)),
            'tanggal_ev_akhir'  => date('Y-m-d', strtotime($req->akhir_event)),
            'tanggal_dft_mulai' => date('Y-m-d', strtotime($req->mulai_daftar)),
            'tanggal_dft_akhir' => date('Y-m-d', strtotime($req->akhir_daftar)),
            'slot_tersedia'     => $req->slot,
            'slot_tersisa'      => $req->slot,
            'kategoriId'        => $req->kategori,
            'foto'              => $namaFoto,
            'status'            => '00',
            'created_user'      => session('username'),
            'created_at'        => \Carbon\Carbon::now()
        );

        $result = $this->model->simpanData('esport', $data);

        return $result;
    }

    public function update(Request $req)
    {
        $id         = $req->id;
        $namaFoto   = $req->fotolama;

        if ($req->file('foto')) {
            File::delete('assets/upload/esport/' . $namaFoto);

            $nama       = preg_replace('/\s+/', '', $req->nama);

            $namaFoto   = $nama . '.' . $req->file('foto')->getClientOriginalExtension();

            $req->file('foto')->move('assets/upload/esport', $namaFoto);
        }

        $data   = array(
            'nama'              => $req->nama,
            'harga'             => $req->harga,
            'deskripsi'         => $req->deskripsi,
            'tanggal_ev_mulai'  => date('Y-m-d', strtotime($req->mulai_event)),
            'tanggal_ev_akhir'  => date('Y-m-d', strtotime($req->akhir_event)),
            'tanggal_dft_mulai' => date('Y-m-d', strtotime($req->mulai_daftar)),
            'tanggal_dft_akhir' => date('Y-m-d', strtotime($req->akhir_daftar)),
            'slot_tersedia'     => $req->slot,
            'slot_tersisa'      => $req->slot,
            'kategoriId'        => $req->kategori,
            'foto'              => $namaFoto,
            'updated_user'      => session('username'),
            'updated_at'        => \Carbon\Carbon::now()
        );

        $result = $this->model->updateData('esport', $data, $id);

        return $result;
    }

    public function delete(Request $req)
    {
        $id     = $req->id;

        $where  = 'id = ' . $id;

        $data   = $this->model->ambilSatuData('esport', $where);

        File::delete('assets/upload/esport/' . $data->foto);

        $result = $this->model->hapusData('esport', $id);

        return $result;
    }
}
