<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CRUDModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
    public function __construct() {
        if (!session()->has('login_state')) {
            Redirect::to('login')->send();
        }

        $this->model    = new CRUDModel();
    }

    public function index()
    {
        $kategori   = $this->model->ambilWhereData('kategori', 'menu = "Product"');

        return view('admin/product/data', compact('kategori'));
    }

    public function input()
    {
        $kategori   = $this->model->ambilWhereData('kategori', 'menu = "Product"');

        return view('admin/product/input', compact('kategori'));
    }

    public function edit($id)
    {
        $kategori   = $this->model->ambilWhereData('kategori', 'menu = "Product"');
        $data       = DB::table('product')
                        ->select(
                            'product.*',
                            'kategori.kategori AS kategori'
                        )
                        ->join('kategori', 'product.kategoriId', 'kategori.id')
                        ->where('product.id', $id)
                        ->first();

        return view('admin/product/edit', compact('kategori', 'data'));
    }

    public function ambilData(Request $req)
    {
        $tipe       = null;
        if ($req->tipe != 'NULL') {
            $tipe       = $req->tipe;

        }

        $kategori   = null;
        if ($req->kategori != 'NULL') {
            $kategori   = $req->kategori;
        }

        $data       = DB::table('product')
                        ->select(
                            'product.*',
                            'kategori.kategori AS kategori'
                        )
                        ->when($tipe, function ($query) use ($tipe) {
                            $query->where('status', $tipe);
                        })
                        ->when($kategori, function ($query) use ($kategori) {
                            $query->where('kategoriId', $kategori);
                        })
                        ->join('kategori', 'product.kategoriId', 'kategori.id')
                        ->get();

        return $data;
    }

    public function publish(Request $req)
    {
        if ($req->file('foto')) {
            $nama       = preg_replace('/\s+/', '', $req->nama);

            $namaFoto   = $nama . '.' . $req->file('foto')->getClientOriginalExtension();

            $req->file('foto')->move('assets/upload/product', $namaFoto);
        }

        $data   = array(
            'nama'          => $req->nama,
            'harga'         => $req->harga,
            'deskripsi'     => $req->deskripsi,
            'kategoriId'    => $req->kategori,
            'foto'          => $namaFoto,
            'status'        => '01',
            'created_user'  => session('username'),
            'created_at'    => \Carbon\Carbon::now()
        );

        $result = $this->model->simpanData('product', $data);

        return $result;
    }

    public function draft(Request $req)
    {
        if ($req->file('foto')) {
            $nama       = preg_replace('/\s+/', '', $req->nama);

            $namaFoto   = $nama . '.' . $req->file('foto')->getClientOriginalExtension();

            $req->file('foto')->move('assets/upload/product', $namaFoto);
        }

        $data   = array(
            'nama'          => $req->nama,
            'harga'         => $req->harga,
            'deskripsi'     => $req->deskripsi,
            'kategoriId'    => $req->kategori,
            'foto'          => $namaFoto,
            'status'        => '00',
            'created_user'  => session('username'),
            'created_at'    => \Carbon\Carbon::now()
        );

        $result = $this->model->simpanData('product', $data);

        return $result;
    }

    public function update(Request $req)
    {
        $id         = $req->id;
        $namaFoto   = $req->fotolama;

        if ($req->file('foto')) {
            File::delete('assets/upload/product/' . $namaFoto);

            $nama       = preg_replace('/\s+/', '', $req->nama);

            $namaFoto   = $nama . '.' . $req->file('foto')->getClientOriginalExtension();

            $req->file('foto')->move('assets/upload/product', $namaFoto);
        }

        $data   = array(
            'nama'          => $req->nama,
            'harga'         => $req->harga,
            'deskripsi'     => $req->deskripsi,
            'kategoriId'    => $req->kategori,
            'foto'          => $namaFoto,
            'updated_user'  => session('username'),
            'updated_at'    => \Carbon\Carbon::now()
        );

        $result = $this->model->updateData('product', $data, $id);

        return $result;
    }

    public function delete(Request $req)
    {
        $id     = $req->id;

        $where  = 'id = ' . $id;

        $data   = $this->model->ambilSatuData('product', $where);

        File::delete('assets/upload/product/' . $data->foto);

        $result = $this->model->hapusData('product', $id);

        return $result;
    }
}
