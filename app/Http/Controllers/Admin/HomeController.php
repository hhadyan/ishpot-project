<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    public function __construct() {
        if (!session()->has('login_state')) {
            Redirect::to('login')->send();
        }
    }

    public function index()
    {
        return view('admin/index');
    }
}
