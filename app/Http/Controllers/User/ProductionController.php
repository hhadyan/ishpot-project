<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CRUDModel;
use Illuminate\Support\Facades\DB;

class ProductionController extends Controller
{
    public function __construct() {
        $this->model    = new CRUDModel();
    }

    public function index()
    {
        $kategori   = $this->model->ambilWhereData('kategori', 'menu = "Product"');

        return view('user/production', compact('kategori'));
    }

    public function getData(Request $req)
    {
        $kategori   = null;
        if ($req->kategori != '0') {
            $kategori   = $req->kategori;
        }

        $data       = DB::table('product')
                        ->select(
                            'product.*',
                            'kategori.kategori AS kategori'
                        )
                        ->when($kategori, function ($query) use ($kategori) {
                            $query->where('kategoriId', $kategori);
                        })
                        ->where('product.status', '01')
                        ->join('kategori', 'product.kategoriId', 'kategori.id')
                        ->get();

        return $data;
    }
}
