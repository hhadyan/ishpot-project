<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CRUDModel;
use Illuminate\Support\Facades\DB;

class CafeController extends Controller
{
    public function __construct() {
        $this->model    = new CRUDModel();
    }

    public function index()
    {
        $kategori   = $this->model->ambilWhereData('kategori', 'menu = "Cafe"');

        return view('user/cafe', compact('kategori'));
    }

    public function getData(Request $req)
    {
        $kategori   = null;
        if ($req->kategori != '0') {
            $kategori   = $req->kategori;
        }

        $data       = DB::table('menu')
                        ->select(
                            'menu.*',
                            'kategori.kategori AS kategori'
                        )
                        ->when($kategori, function ($query) use ($kategori) {
                            $query->where('kategoriId', $kategori);
                        })
                        ->where('menu.status', '01')
                        ->join('kategori', 'menu.kategoriId', 'kategori.id')
                        ->get();

        return $data;
    }
}
