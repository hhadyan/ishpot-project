<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CRUDModel;
use Illuminate\Support\Facades\DB;

class EsportController extends Controller
{
    public function __construct() {
        $this->model    = new CRUDModel();
    }

    public function index()
    {
        $kategori   = $this->model->ambilWhereData('kategori', 'menu = "Esport"');

        return view('user/esport', compact('kategori'));
    }

    public function getData(Request $req)
    {
        $kategori   = null;
        if ($req->kategori != '0') {
            $kategori   = $req->kategori;
        }

        $data       = DB::table('esport')
                        ->select(
                            'esport.*',
                            'kategori.kategori AS kategori'
                        )
                        ->when($kategori, function ($query) use ($kategori) {
                            $query->where('kategoriId', $kategori);
                        })
                        ->where('esport.status', '01')
                        ->join('kategori', 'esport.kategoriId', 'kategori.id')
                        ->get();

        return $data;
    }
}
