<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CRUDModel extends Model
{
    function simpanData($tabel, $data)
    {
        DB::beginTransaction();
        try {
            DB::table($tabel)
                ->insert($data);

            DB::commit();

            $result = array(
                'msg'       => 'success',
                'msg_data'  => 'Berhasil Menyimpan Data'
            );

            return $result;
        } catch (\Throwable $e) {
            DB::rollBack();

            $result = array(
                'msg'       => 'error',
                'msg_data'  => $e->getMessage()
            );

            return $result;
        }
    }

    function updateData($tabel, $data, $kondisi)
    {
        DB::beginTransaction();
        try {
            DB::table($tabel)
                ->where('id', $kondisi)
                ->update($data);

            DB::commit();

            $result = array(
                'msg'       => 'success',
                'msg_data'  => 'Berhasil Merubah Data'
            );

            return $result;
        } catch (\Throwable $e) {
            DB::rollBack();

            $result = array(
                'msg'       => 'error',
                'msg_data'  => $e->getMessage()
            );

            return $result;
        }
    }

    function hapusData($tabel, $kondisi)
    {
        DB::beginTransaction();
        try {
            DB::table($tabel)
                ->where($tabel.'.id', $kondisi)
                ->delete();

            DB::commit();

            $result = array(
                'msg'       => 'success',
                'msg_data'  => 'Berhasil Menghapus Data'
            );

            return $result;
        } catch (\Throwable $e) {
            DB::rollBack();

            $result = array(
                'msg'       => 'error',
                'msg_data'  => $e->getMessage()
            );

            return $result;
        }
    }

    function publish($tabel, $kondisi)
    {
        DB::beginTransaction();
        try {
            DB::table($tabel)
                ->where($tabel.'.id', $kondisi)
                ->update([
                    'status'        => '01',
                    'updated_user'  => session('username'),
                    'updated_at'    => \Carbon\Carbon::now()
                ]);

            DB::commit();

            $result = array(
                'msg'       => 'success',
                'msg_data'  => 'Berhasil Merubah Data'
            );

            return $result;
        } catch (\Throwable $e) {
            DB::rollBack();

            $result = array(
                'msg'       => 'error',
                'msg_data'  => $e->getMessage()
            );

            return $result;
        }
    }

    function draft($tabel, $kondisi)
    {
        DB::beginTransaction();
        try {
            DB::table($tabel)
                ->where($tabel.'.id', $kondisi)
                ->update([
                    'status'        => '00',
                    'updated_user'  => session('username'),
                    'updated_at'    => \Carbon\Carbon::now()
                ]);

            DB::commit();

            $result = array(
                'msg'       => 'success',
                'msg_data'  => 'Berhasil Merubah Data'
            );

            return $result;
        } catch (\Throwable $e) {
            DB::rollBack();

            $result = array(
                'msg'       => 'error',
                'msg_data'  => $e->getMessage()
            );

            return $result;
        }
    }

    function ambilData($tabel)
    {
        try {
            $result = DB::table($tabel)
                        ->get();

            return $result;
        } catch (\Throwable $e) {
            $result = array(
                'msg'       => 'error',
                'msg_data'  => $e->getMessage()
            );

            return $result;
        }
    }

    function ambilWhereData($tabel, $kondisi)
    {
        try {
            $result = DB::table($tabel)
                        ->whereRaw($kondisi)
                        ->get();

            return $result;
        } catch (\Throwable $e) {
            $result = array(
                'msg'       => 'error',
                'msg_data'  => $e->getMessage()
            );

            return $result;
        }
    }

    function ambilSatuData($tabel, $kondisi)
    {
        try {
            $result = DB::table($tabel)
                        ->whereRaw($kondisi)
                        ->first();

            return $result;
        } catch (\Throwable $e) {
            $result = array(
                'msg'       => 'error',
                'msg_data'  => $e->getMessage()
            );

            return $result;
        }
    }
}
