@extends('admin.layout.layout')
@section('header-content')
    <div class="header-body">
        <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
                <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                        <li class="breadcrumb-item"><a href="{{ url('/wp-admin') }}"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Akun Admin</li>
                    </ol>
                </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="col-xl-12 order-xl-1">
        <div class="card">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="mb-0">Daftar Akun Admin </h3>
                    </div>
                    <div class="col-4 text-right">
                        <a href="{{ url('/admin/insert') }}" class="btn btn-primary">Tambah Akun</a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        <select class="form-control" id="filter-role">
                            <option value="NULL">Semua Peran</option>
                            <option value="01">Super Admin</option>
                            <option value="02">Admin</option>
                            <option value="03">Penulis</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <select class="form-control" id="filter-aktif">
                            <option value="NULL">Semua Status</option>
                            <option value="01">Aktif</option>
                            <option value="00">Tidak Aktif</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <button id="btnCari" class="btn btn-primary"><i class="fas fa-search"></i> Cari</button>
                    </div>
                </div>

                <table
                    id="data-tabel"
                    data-toggle="table"
                    data-toolbar="#filter"
                    data-search="true"
                    data-height="640"
                    data-page-size="25"
                    data-pagination="true">
                    <thead>
                        <tr>
                            <th data-field="id" data-visible="false"></th>
                            <th data-formatter="numberFormatter">No</th>
                            <th data-field="nama" data-sortable="true">Nama Akun</th>
                            <th data-field="username" data-sortable="true">Username</th>
                            <th data-field="role" data-formatter="peranFormatter">Peran</th>
                            <th data-field="status" data-formatter="statusFormatter">Status</th>
                            <th data-field="status" data-formatter="aktifFormatter">Ubah Status</th>
                            <th data-field="status" data-formatter="aksiFormatter"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js-raw')
    <script>
        window.data = {
            datatable   : [],
            tabel       : ''
        }

        $(document).ready(function () {
            getData('0');
        });

        $(document).on('click', '.btn-aktif', function() {
            var id  = $(this).attr('data-id');

            APIpublish('admin', id, function() {
                getData('0');
            });
        })

        $(document).on('click', '.btn-deaktif', function() {
            var id  = $(this).attr('data-id');

            APIdraft('admin', id, function() {
                getData('0');
            });
        })

        $(document).on('click', '.btn-delete', function() {
            var id  = $(this).attr('data-id');

            APIdelete('admin', id, function() {
                getData('0');
            });
        })

        $(document).on('click', '.btn-edit', function() {
            var id  = $(this).attr('data-id');

            window.open("{{ url('/admin/edit') }}" + "/" + id, "_blank");
        })

        $('#btnCari').on('click', function() {
            getData('1');
        })

        function getData(button) {
            var tabel       = '#data-tabel';
            var url         = '{{ url("/admin/get") }}';

            var role        = $('#filter-role').val();
            var status      = $('#filter-aktif').val();

            var data    = {
                role: role,
                status: status
            };

            APIshowFiltered(url, tabel, data, button);
        }

        function aksiFormatter(value, row, index) {
            if (value == '01') {
                return '<button data-id="' + row.id + '" class="btn-sm btn-primary btn-edit">' +
                            '<i class="fas fa-user-edit"></i> Edit' +
                        '</button>';
            } else {
                if ({{ session('role') }} == '01' && {{ session('id')}} != row.id) {
                    return '<button data-id="' + row.id + '" class="btn-sm btn-primary btn-edit">' +
                                '<i class="fas fa-user-edit"></i> Edit' +
                            '</button> ' +
                            '<button data-id="' + row.id + '" class="btn-sm btn-secondary btn-delete">' +
                                '<i class="fas fa-trash"></i> Delete' +
                            '</button>';
                } else {
                    return '<button data-id="' + row.id + '" class="btn-sm btn-primary btn-edit">' +
                                '<i class="fas fa-user-edit"></i> Edit' +
                            '</button>';
                }

            }
        }

        function aktifFormatter(value, row, index) {
            if ({{ session('id')}} != row.id) {
                if (value == '01') {
                    return '<button data-id="' + row.id + '" class="btn-sm btn-secondary btn-deaktif">' +
                                'Non-Aktifkan' +
                            '</button>';
                } else {
                    return '<button data-id="' + row.id + '" class="btn-sm btn-primary btn-aktif">' +
                                'Aktifkan' +
                            '</button>';
                }
            }
        }

        function statusFormatter(value, row, index) {
            if (value == '01') {
                return "Aktif";
            } else {
                return "Tidak Aktif";
            }
        }

        function peranFormatter(value, row, index) {
            if (value == '01') {
                return "Super Admin";
            } else if (value == '02') {
                return "Admin";
            } else {
                return "Penulis"
            }
        }
    </script>
@endsection
