@extends('admin.layout.layout')
@section('header-content')
    <div class="header-body">
        <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
                <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                        <li class="breadcrumb-item"><a href="{{ url('/wp-admin') }}"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Admin</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="col-xl-12 order-xl-1">
        <div class="card">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="mb-0">Edit Akun Admin </h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form enctype="multipart/form-data" method="post" id="formData">
                    <input type="hidden" id="input-id" value="{{ $data->id }}">
                    <input type="hidden" id="input-passlama" value="{{ $data->password }}">
                    <h6 class="heading-small text-muted mb-4">Data Admin</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-nama">Nama</label>
                                    <input type="text" id="input-nama" class="form-control" value="{{ $data->nama }}">
                                    <small id="alert-nama" class="text-warning" style="display:none;">Nama tidak Boleh Kosong!</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-user">Username</label>
                                    <input type="text" id="input-user" class="form-control" value="{{ $data->username }}">
                                    <small id="alert-user" class="text-warning" style="display:none;">Username tidak Boleh Kosong!</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-nohp">Nomor Whatsapp</label>
                                    <input type="text" id="input-nohp" onkeypress="return hanyaAngka(event)" class="form-control" value="{{ $data->no_hp }}">
                                    <small id="alert-nohp" class="text-warning" style="display:none;">Nomor Whatsapp tidak Boleh Kosong!</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-password">Password</label>
                                    <input type="password" id="input-pass" class="form-control">
                                    <small id="alert-password" class="text-warning" style="display:none;">Password tidak Boleh Kosong!</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-role">Role</label>
                                    <select id="input-role" class="form-control" data-toggle="select">
                                        <option value="NULL">Pilih Peran</option>
                                        <option value="01" {{ ($data->role == '01') ? 'selected' : '' }} >Super Admin</option>
                                        <option value="02" {{ ($data->role == '02') ? 'selected' : '' }} >Admin</option>
                                        <option value="03" {{ ($data->role == '03') ? 'selected' : '' }} >Penulis</option>
                                    </select>
                                    <small id="alert-role" class="text-warning" style="display:none;">Silahkan Pilih Peran!</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <input type="button" id="btnSave" class="btn btn-primary" value="Simpan">
            </div>
        </div>
    </div>
@endsection

@section('js-raw')
    <script>
        let editor;
        window.cek  = 0;

        function validasi() {
            var nama        = $('#input-nama').val();
            var user        = $('#input-user').val();
            var pass        = $('#input-pass').val();
            var nohp        = $('#input-nohp').val();
            var role        = $('#input-role').val();

            if (nama == '') {
                $('#alert-nama').show();
                window.cek = 0;
            } else {
                $('#alert-nama').hide();
            }

            if (user == '') {
                $('#alert-user').show();
                window.cek = 0;
            } else {
                $('#alert-user').hide();
            }

            if (pass == '') {
                $('#alert-pass').show();
                window.cek = 0;
            } else {
                $('#alert-pass').hide();
            }

            if (nohp == '') {
                $('#alert-nohp').show();
                window.cek = 0;
            } else {
                $('#alert-nohp').hide();
            }

            if (role == 'NULL') {
                $('#alert-role').show();
                window.cek = 0;
            } else {
                $('#alert-role').hide();
            }

            if (nama != '' && user != '' && pass != '' && nohp != '' && role != 'NULL') {
                window.cek = 1;
            }
        }

        $('#btnSave').on('click', function() {
            var data        = new FormData();

            data.append('id', $('#input-id').val());
            data.append('passlama', $('#input-passlama').val());
            data.append('nama', $('#input-nama').val());
            data.append('user', $('#input-user').val());
            data.append('pass', $('#input-pass').val());
            data.append('nohp', $('#input-nohp').val());
            data.append('role', $('#input-role').val());

            validasi();

            var url     = "{{ url('/admin/update') }}";

            if (window.cek != 0) {
                APIupdate(url, data);
            }
        })

        $('#input-nama, #input-user, #input-pass, #input-nohp').keyup(function() {
            validasi();
        });

        $('#input-role').change(function() {
            validasi();
        });
    </script>
@endsection
