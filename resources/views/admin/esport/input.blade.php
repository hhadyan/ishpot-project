@extends('admin.layout.layout')
@section('header-content')
    <div class="header-body">
        <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
                <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                        <li class="breadcrumb-item"><a href="{{ url('/wp-admin') }}"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/esport') }}">E-Sport</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Input</li>
                    </ol>
                </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="col-xl-12 order-xl-1">
        <div class="card">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="mb-0">Tambah Event E-Sport </h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form enctype="multipart/form-data" method="post" id="formData">
                    <h6 class="heading-small text-muted mb-4">Foto Event</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <img src="#" id="preview-img" style="display:none;">
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <input type="file" id="input-foto" accept="img/*" class="form-control">
                                    <div class="text-muted font-italic">
                                        <small id="note-foto">Gunakan Gambar dengan Dimensi <strong>336x417</strong> Agar Tampilan Optimal</small>
                                        <small id="alert-foto" class="text-warning" style="display:none;">Anda Belum Mengunggah Foto!</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                        </div>
                    </div>
                    <hr class="my-4" />
                    <h6 class="heading-small text-muted mb-4">Detail Event</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-nama">Nama</label>
                                    <input type="text" id="input-nama" class="form-control">
                                    <small id="alert-nama" class="text-warning" style="display:none;">Nama tidak Boleh Kosong!</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-kategori">Kategori</label>
                                    <select id="input-kat" class="form-control" data-toggle="select">
                                        <option value="NULL">Pilih Kategori</option>
                                        @foreach ($kategori as $kat)
                                            <option value="{{ $kat->id }}">{{ $kat->kategori }}</option>
                                        @endforeach
                                    </select>
                                    <small id="alert-kategori" class="text-warning" style="display:none;">Silahkan Pilih Kategori!</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-harga">Biaya Pendaftaran</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp</span>
                                        </div>
                                        <input type="text" id="input-harga" onkeypress="return hanyaAngka(event)" class="form-control">
                                    </div>
                                    <small id="alert-harga" class="text-warning" style="display:none;">Biaya Pendaftaran tidak Boleh Kosong!</small>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-harga">Jumlah Slot</label>
                                    <input type="number" id="input-slot" onkeypress="return hanyaAngka(event)" class="form-control">
                                    <small id="alert-slot" class="text-warning" style="display:none;">Jumlah Slot tidak Boleh Kosong!</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">Tanggal Pendaftaran</label>
                                    <input class="form-control" name="daterange" id="input-daftar" type="text">
                                    <small id="alert-daftar" class="text-warning" style="display:none;">Tanggal Pendaftaran tidak Boleh Kosong!</small>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">Tanggal Event</label>
                                    <input class="form-control" name="daterange" id="input-event" type="text">
                                    <small id="alert-event" class="text-warning" style="display:none;">Tanggal Event tidak Boleh Kosong!</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Deskripsi</label>
                                    <textarea rows="4" id="input-deskripsi" class="editor"></textarea>
                                    <small id="alert-deskripsi" class="text-warning" style="display:none;">Deskripsi tidak Boleh Kosong!</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <input type="button" id="btnPublish" class="btn btn-primary" value="Publish">
                <input type="button" id="btnDraft" class="btn btn-secondary" value="Draft">
            </div>
        </div>
    </div>
@endsection

@section('js-raw')
    <script>
        let editor;
        window.cek  = 0;

        $(document).ready(function () {
            var start = moment();
            var end = moment().add(7, 'days');

            $('input[name="daterange"]').daterangepicker({
                opens: 'right',
                startDate: start,
                endDate: end,
                locale: {
                    format: 'DD/MM/YYYY'
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,'month').endOf('month')]
                }
            });

            ClassicEditor
                .create( document.querySelector( '#input-deskripsi' ) )
                .then( newEditor => {
                    editor = newEditor;
                    editor.editing.view.document.on( 'keyup', ( evt, data ) => {
                        if ( editor.getData() != '') {
                            $('#alert-deskripsi').hide();
                        } else {
                            window.cek = 0;
                            $('#alert-deskripsi').show();
                        }
                    } )
                } )
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview-img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);

                $('#preview-img').show();
            }
        }

        function validasi() {
            var nama        = $('#input-nama').val();
            var harga       = $('#input-harga').val();
            var slot        = $('#input-slot').val();
            var daftar      = $('#input-daftar').val();
            var event       = $('#input-event').val();
            var kategori    = $('#input-kat').val();
            var deskripsi   = editor.getData();
            var foto        = $('#input-foto')[0].files[0];

            if (nama == '') {
                $('#alert-nama').show();
                window.cek = 0;
            } else {
                $('#alert-nama').hide();
            }

            if (harga == '') {
                $('#alert-harga').show();
                window.cek = 0;
            } else {
                $('#alert-harga').hide();
            }

            if (slot == '') {
                $('#alert-slot').show();
                window.cek = 0;
            } else {
                $('#alert-slot').hide();
            }

            if (daftar == '') {
                $('#alert-daftar').show();
                window.cek = 0;
            } else {
                $('#alert-daftar').hide();
            }

            if (event == '') {
                $('#alert-event').show();
                window.cek = 0;
            } else {
                $('#alert-event').hide();
            }

            if (kategori == 'NULL') {
                $('#alert-kategori').show();
                window.cek = 0;
            } else {
                $('#alert-kategori').hide();
            }

            if (deskripsi == '') {
                $('#alert-deskripsi').show();
                window.cek = 0;
            } else {
                $('#alert-deskripsi').hide();
            }

            if (foto == undefined) {
                $('#alert-foto').show();
                $('#note-foto').hide();
                window.cek = 0;
            } else {
                $('#alert-foto').hide();
                $('#note-foto').show();
            }

            if (nama != '' && harga != '' && slot != '' && daftar != '' && event != '' && kategori != 'NULL' && deskripsi != '' && foto != undefined) {
                window.cek = 1;
            }
        }

        $('#btnPublish').on('click', function() {
            var data        = new FormData();

            var daftar      = $("#input-daftar").val();
            var daftar_split= daftar.split('-');

            var mulai_daftar= $.trim(daftar_split[0].replace(/\//ig, '-'));
            var akhir_daftar= $.trim(daftar_split[1].replace(/\//ig, '-'));

            var event       = $("#input-event").val();
            var event_split = event.split('-');

            var mulai_event = $.trim(event_split[0].replace(/\//ig, '-'));
            var akhir_event = $.trim(event_split[1].replace(/\//ig, '-'));

            data.append('foto', $('#input-foto')[0].files[0]);
            data.append('nama', $('#input-nama').val());
            data.append('harga', $('#input-harga').val());
            data.append('slot', $('#input-slot').val());
            data.append('mulai_daftar', mulai_daftar);
            data.append('akhir_daftar', akhir_daftar);
            data.append('mulai_event', mulai_event);
            data.append('akhir_event', akhir_event);
            data.append('kategori', $('#input-kat').val());
            data.append('deskripsi', editor.getData());

            validasi();

            var url     = "{{ url('/esport/publish') }}";

            if (window.cek != 0) {
                APIsimpan(url, data);
            }
        })

        $('#btnDraft').on('click', function() {
            var data        = new FormData();

            var daftar      = $("#input-daftar").val();
            var daftar_split= daftar.split('-');

            var mulai_daftar= $.trim(daftar_split[0].replace(/\//ig, '-'));
            var akhir_daftar= $.trim(daftar_split[1].replace(/\//ig, '-'));

            var event       = $("#input-event").val();
            var event_split = event.split('-');

            var mulai_event = $.trim(event_split[0].replace(/\//ig, '-'));
            var akhir_event = $.trim(event_split[1].replace(/\//ig, '-'));

            data.append('foto', $('#input-foto')[0].files[0]);
            data.append('nama', $('#input-nama').val());
            data.append('harga', $('#input-harga').val());
            data.append('slot', $('#input-slot').val());
            data.append('mulai_daftar', mulai_daftar);
            data.append('akhir_daftar', akhir_daftar);
            data.append('mulai_event', mulai_event);
            data.append('akhir_event', akhir_event);
            data.append('kategori', $('#input-kat').val());
            data.append('deskripsi', editor.getData());

            validasi();

            var url     = "{{ url('/esport/draft') }}";

            if (window.cek != 0) {
                APIsimpan(url, data);
            }
        })

        $("#input-foto").change(function() {
            readURL(this);
            validasi();
        });

        $('#input-nama, #input-harga, #input-slot').keyup(function() {
            validasi();
        });

        $('#input-kat, #input-slot').change(function() {
            validasi();
        });
    </script>
@endsection
