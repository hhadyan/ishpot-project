@extends('admin.layout.layout')
@section('header-content')
    <div class="header-body">
        <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
                <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                        <li class="breadcrumb-item"><a href="{{ url('/wp-admin') }}"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Sport</li>
                    </ol>
                </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="col-xl-12 order-xl-1">
        <div class="card">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="mb-0">Daftar Event E-Sport </h3>
                    </div>
                    <div class="col-4 text-right">
                        <a href="{{ url('/esport/insert') }}" class="btn btn-primary">Tambah Post</a>
                    </div>
                </div>
            </div>

            <div class="card-body">

                <div class="row">
                    <div class="col-md-2">
                        <select class="form-control" id="filter-aktif">
                            <option value="NULL">Semua Tipe</option>
                            <option value="01">Publish</option>
                            <option value="00">Draft</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <select class="form-control" id="filter-kategori" data-toggle="select">
                            <option value="NULL">Semua Kategori</option>
                            @foreach ($kategori as $kat)
                                <option value="{{ $kat->id }}">{{ $kat->kategori }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-2">
                        <button id="btnCari" class="btn btn-primary"><i class="fas fa-search"></i> Cari</button>
                    </div>
                </div>

                <div id="filter">
                    <div class="row">
                        <div class="col">
                            <button id="btnDraft" class="btn-sm btn-secondary">Draft</button>
                            <button id="btnPublish" class="btn-sm btn-secondary">Publish</button>
                        </div>
                    </div>
                </div>

                <table
                    id="data-tabel"
                    data-toggle="table"
                    data-toolbar="#filter"
                    data-search="true"
                    data-height="640"
                    data-page-size="25"
                    data-pagination="true">
                    <thead>
                        <tr>
                            <th data-checkbox="true"></th>
                            <th data-field="id" data-visible="false"></th>
                            <th data-field="slot_tersedia" data-visible="false"></th>
                            <th data-field="slot_tersisa" data-visible="false"></th>
                            <th data-field="tanggal_ev_mulai" data-visible="false"></th>
                            <th data-field="tanggal_ev_akhir" data-visible="false"></th>
                            <th data-field="tanggal_dft_mulai" data-visible="false"></th>
                            <th data-field="tanggal_dft_akhir" data-visible="false"></th>
                            <th data-formatter="numberFormatter">No</th>
                            <th data-field="nama" data-sortable="true">Nama Menu</th>
                            <th data-field="kategori" data-sortable="true">Kategori</th>
                            <th data-field="harga" data-formatter="moneyFormatter">Biaya Pendaftaran</th>
                            <th data-field="slot" data-formatter="slotFormatter">Slot</th>
                            <th data-field="tgl_daftar" data-formatter="daftarFormatter">Tanggal Pendaftaran</th>
                            <th data-field="tgl_event" data-formatter="eventFormatter">Tanggal Event</th>
                            <th data-field="status" data-formatter="detailFormatter">Status</th>
                            <th data-field="status" data-formatter="aktifFormatter">Tampil</th>
                            <th data-field="status" data-formatter="aksiFormatter"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js-raw')
    <script>
        window.data = {
            datatable   : [],
            tabel       : ''
        }

        $(document).ready(function () {
            getData('0');
        });

        $(document).on('click', '.btn-draft', function() {
            var id  = $(this).attr('data-id');

            APIdraft('esport', id, function() {
                getData('0');
            });
        })

        $(document).on('click', '.btn-publish', function() {
            var id  = $(this).attr('data-id');

            APIpublish('esport', id, function() {
                getData('0');
            });
        })

        $(document).on('click', '.btn-delete', function() {
            var id  = $(this).attr('data-id');

            swal({
                title: "Anda Yakin untuk Menghapus Data Ini?",
                text: "Data yang telah dihapus tidak dapat dikembalikan!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText:'Hapus',
                confirmButtonColor: '#d33',
                cancelButtonText:'Kembali'
            }).then((respon) => {
                if (respon.value == true) {
                    $.ajax({
                        type: "post",
                        url: "{{ url('/esport/delete') }}",
                        data: {
                            id: id
                        },
                        beforeSend: function () {
                            swal({
                                title:'Menghapus Data',
                                text: 'Mohon Tunggu...',
                                allowOutsideClick: false,
                                onOpen: function() {
                                    swal.showLoading()
                                }
                            })
                        },
                        success: function (data) {
                            if (data.msg == 'success') {
                                swal.close();

                                swal({
                                    type:'success',
                                    title:'Berhasil Menghapus Data',
                                    allowOutsideClick: true
                                })

                                getData('0');
                            } else {
                                swal.close();

                                swal({
                                    type: 'error',
                                    title: 'Gagal Menghapus Data',
                                    text: data.msg_data,
                                    allowOutsideClick: true
                                })
                            }
                        },
                        error: function (data) {
                            swal.close();

                            swal({
                            type: 'error',
                            title: 'Gagal Menghapus Data',
                            text: data.msg_data,
                            allowOutsideClick: true
                            })
                        }
                    });
                }
            })


        })

        $(document).on('click', '.btn-edit', function() {
            var id  = $(this).attr('data-id');

            window.open("{{ url('/esport/edit') }}" + "/" + id, "_blank");
        })

        $('#btnCari').on('click', function() {
            getData('1');
        })

        $('#btnDraft').on('click', function() {
            var datatable       = $('#data-tabel').bootstrapTable('getSelections');
            window.data.datatbl = datatable;
            window.data.tabel   = 'esport';

            if (datatable.length > 0) {
                $.ajax({
                    type: "POST",
                    url: "{{ url('/draft-all') }}",
                    data: JSON.stringify(window.data),
                    dataType: "text",
                    contenType : "application/json",
                    beforeSend: function () {
                        swal({
                            title:'Merubah Status',
                            text: 'Mohon Tunggu...',
                            allowOutsideClick: false,
                            onOpen: function() {
                                swal.showLoading()
                            }
                        })
                    },
                    success: function (data) {
                        swal({
                            type:'success',
                            title:'Berhasil Merubah Status',
                            allowOutsideClick: true
                        })

                        getData('0');
                    },
                    error: function (data) {
                        swal.close();

                        swal({
                        type: 'error',
                        title: 'Gagal Merubah Data',
                        text: data.msg_data,
                        allowOutsideClick: true
                        })
                    }
                });
            } else {
                swal({
                    type:'error',
                    title:'Silahkan Pilih Data Terlebih Dahulu!',
                    allowOutsideClick: true
                })
            }
        })

        $('#btnPublish').on('click', function() {
            var datatable       = $('#data-tabel').bootstrapTable('getSelections');
            window.data.datatbl = datatable;
            window.data.tabel   = 'esport';

            if (datatable.length > 0) {
                $.ajax({
                    type: "POST",
                    url: "{{ url('/publish-all') }}",
                    data: JSON.stringify(window.data),
                    dataType: "text",
                    contenType : "application/json",
                    beforeSend: function () {
                        swal({
                            title:'Merubah Status',
                            text: 'Mohon Tunggu...',
                            allowOutsideClick: false,
                            onOpen: function() {
                                swal.showLoading()
                            }
                        })
                    },
                    success: function (data) {
                        swal({
                            type:'success',
                            title:'Berhasil Merubah Status',
                            allowOutsideClick: true
                        })

                        getData('0');
                    },
                    error: function (data) {
                        swal.close();

                        swal({
                        type: 'error',
                        title: 'Gagal Merubah Data',
                        text: data.msg_data,
                        allowOutsideClick: true
                        })
                    }
                });
            } else {
                swal({
                    type:'error',
                    title:'Silahkan Pilih Data Terlebih Dahulu!',
                    allowOutsideClick: true
                })
            }
        })

        function getData(button) {
            var tabel       = '#data-tabel';
            var url         = '{{ url("/esport/get") }}';

            var tipe        = $('#filter-aktif').val();
            var kategori    = $('#filter-kategori').val();

            var data    = {
                tipe: tipe,
                kategori: kategori
            };

            APIshowFiltered(url, tabel, data, button);
        }

        function aksiFormatter(value, row, index) {
            if (value == '01') {
                return '<button data-id="' + row.id + '" class="btn-sm btn-primary btn-edit">' +
                            '<i class="fas fa-user-edit"></i> Edit' +
                        '</button>';
            } else {
                return '<button data-id="' + row.id + '" class="btn-sm btn-primary btn-edit">' +
                            '<i class="fas fa-user-edit"></i> Edit' +
                        '</button> ' +
                        '<button data-id="' + row.id + '" class="btn-sm btn-secondary btn-delete">' +
                            '<i class="fas fa-trash"></i> Delete' +
                        '</button>';
            }
        }

        function aktifFormatter(value, row, index) {
            if (value == '01') {
                return '<button data-id="' + row.id + '" class="btn-sm btn-secondary btn-draft">' +
                            'Draft' +
                        '</button>';
            } else {
                return '<button data-id="' + row.id + '" class="btn-sm btn-primary btn-publish">' +
                            'Publish' +
                        '</button>';
            }
        }

        function detailFormatter(value, row, index) {
            if (value == '01') {
                return "Published";
            } else {
                return "Drafted";
            }
        }

        function slotFormatter(value, row, index) {
            return row.slot_tersisa + '/' + row.slot_tersedia;
        }

        function daftarFormatter(value, row, index) {
            return localeDate(row.tanggal_dft_mulai) + ' s/d ' + localeDate(row.tanggal_dft_akhir);
        }

        function eventFormatter(value, row, index) {
            return localeDate(row.tanggal_ev_mulai) + ' s/d ' + localeDate(row.tanggal_ev_akhir);
        }
    </script>
@endsection
