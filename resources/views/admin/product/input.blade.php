@extends('admin.layout.layout')
@section('header-content')
    <div class="header-body">
        <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
                <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                        <li class="breadcrumb-item"><a href="{{ url('/wp-admin') }}"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/product') }}">Production House</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Input</li>
                    </ol>
                </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="col-xl-12 order-xl-1">
        <div class="card">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="mb-0">Tambah Produk</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form enctype="multipart/form-data" method="post" id="formData">
                    <h6 class="heading-small text-muted mb-4">Foto Produk</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <img src="#" id="preview-img" style="display:none;">
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <input type="file" id="input-foto" accept="img/*" class="form-control">
                                    <div class="text-muted font-italic">
                                        <small id="note-foto">Gunakan Gambar dengan Dimensi <strong>336x417</strong> Agar Tampilan Optimal</small>
                                        <small id="alert-foto" class="text-warning" style="display:none;">Anda Belum Mengunggah Foto!</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                        </div>
                    </div>
                    <hr class="my-4" />
                    <h6 class="heading-small text-muted mb-4">Detail Produk</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-nama">Nama</label>
                                    <input type="text" id="input-nama" class="form-control">
                                    <small id="alert-nama" class="text-warning" style="display:none;">Nama tidak Boleh Kosong!</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-harga">Harga</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp</span>
                                        </div>
                                        <input type="text" id="input-harga" onkeypress="return hanyaAngka(event)" class="form-control">
                                    </div>
                                    <small id="alert-harga" class="text-warning" style="display:none;">Harga tidak Boleh Kosong!</small>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-kategori">Kategori</label>
                                    <select id="input-kat" class="form-control" data-toggle="select">
                                        <option value="NULL">Pilih Kategori</option>
                                        @foreach ($kategori as $kat)
                                            <option value="{{ $kat->id }}">{{ $kat->kategori }}</option>
                                        @endforeach
                                    </select>
                                    <small id="alert-kategori" class="text-warning" style="display:none;">Silahkan Pilih Kategori!</small>
                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Deskripsi</label>
                                    <textarea rows="4" id="input-deskripsi" class="editor"></textarea>
                                    <small id="alert-deskripsi" class="text-warning" style="display:none;">Deskripsi tidak Boleh Kosong!</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <input type="button" id="btnPublish" class="btn btn-primary" value="Publish">
                <input type="button" id="btnDraft" class="btn btn-secondary" value="Draft">
            </div>
        </div>
    </div>
@endsection

@section('js-raw')
    <script>
        let editor;
        window.cek  = 0;

        $(document).ready(function () {
            ClassicEditor
                .create( document.querySelector( '#input-deskripsi' ) )
                .then( newEditor => {
                    editor = newEditor;
                    editor.editing.view.document.on( 'keyup', ( evt, data ) => {
                        if ( editor.getData() != '') {
                            $('#alert-deskripsi').hide();
                        } else {
                            window.cek = 0;
                            $('#alert-deskripsi').show();
                        }
                    } )
                } )
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview-img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);

                $('#preview-img').show();
            }
        }

        function validasi() {
            var nama        = $('#input-nama').val();
            var harga       = $('#input-harga').val();
            var kategori    = $('#input-kat').val();
            var deskripsi   = editor.getData();
            var foto        = $('#input-foto')[0].files[0];

            if (nama == '') {
                $('#alert-nama').show();
                window.cek = 0;
            } else {
                $('#alert-nama').hide();
            }

            if (harga == '') {
                $('#alert-harga').show();
                window.cek = 0;
            } else {
                $('#alert-harga').hide();
            }

            if (kategori == 'NULL') {
                $('#alert-kategori').show();
                window.cek = 0;
            } else {
                $('#alert-kategori').hide();
            }

            if (deskripsi == '') {
                $('#alert-deskripsi').show();
                window.cek = 0;
            } else {
                $('#alert-deskripsi').hide();
            }

            if (foto == undefined) {
                $('#alert-foto').show();
                $('#note-foto').hide();
                window.cek = 0;
            } else {
                $('#alert-foto').hide();
                $('#note-foto').show();
            }

            if (nama != '' && harga != '' && kategori != 'NULL' && deskripsi != '' && foto != undefined) {
                window.cek = 1;
            }
        }

        $('#btnPublish').on('click', function() {
            var data        = new FormData();

            data.append('foto', $('#input-foto')[0].files[0]);
            data.append('nama', $('#input-nama').val());
            data.append('harga', $('#input-harga').val());
            data.append('kategori', $('#input-kat').val());
            data.append('deskripsi', editor.getData());

            validasi();

            var url     = "{{ url('/product/publish') }}";

            if (window.cek != 0) {
                APIsimpan(url, data);
            }
        })

        $('#btnDraft').on('click', function() {
            var data        = new FormData();

            data.append('foto', $('#input-foto')[0].files[0]);
            data.append('nama', $('#input-nama').val());
            data.append('harga', $('#input-harga').val());
            data.append('kategori', $('#input-kat').val());
            data.append('deskripsi', editor.getData());

            validasi();

            var url     = "{{ url('/product/draft') }}";

            if (window.cek != 0) {
                APIsimpan(url, data);
            }
        })

        $("#input-foto").change(function() {
            readURL(this);
            validasi();
        });

        $('#input-nama, #input-harga').keyup(function() {
            validasi();
        });

        $('#input-kat').change(function() {
            validasi();
        });
    </script>
@endsection
