<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ishpot Indonesia | Admin</title>
    @include('admin.layout.css')
</head>

<body class="bg-default">
    <!-- Main content -->
    <div class="main-content">
        <!-- Header -->
        <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
            <div class="container">
                <div class="header-body text-center mb-7">
                    <div class="row justify-content-center">
                        <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                            <h1 class="text-white">Login Ishpot Indonesia</h1>
                            <p class="text-lead text-white">Masukkan Username dan Password untuk Masuk ke Panel Admin
                                Ishpot Indonesia</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="separator separator-bottom separator-skew zindex-100">
                <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                    xmlns="http://www.w3.org/2000/svg">
                    <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                </svg>
            </div>
        </div>
        <!-- Page content -->
        <div class="container mt--8 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <form role="form">
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                        </div>
                                        <input class="form-control" id="input-user" placeholder="Username" type="text">
                                    </div>
                                    <small id="alert-user" class="text-warning" style="display:none;">Username tidak Boleh Kosong!</small>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control" id="input-pass" placeholder="Password" type="password">
                                    </div>
                                    <small id="alert-pass" class="text-warning" style="display:none;">Password tidak Boleh Kosong!</small>
                                </div>
                                <div class="text-center">
                                    <input type="button" id="btnLogin" class="btn btn-primary my-4" value="Masuk">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer class="py-5" id="footer-main">
        <div class="container">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        &copy; {{ date('Y') }} <a href="#" class="font-weight-bold ml-1" target="_blank">Ishpot
                            Indonesia</a>
                    </div>
                </div>
                <div class="col-xl-6">
                    <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="{{ url('assets/admin/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ url('assets/admin/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('assets/admin/vendor/js-cookie/js.cookie.js') }}"></script>
    <script src="{{ url('assets/admin/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script src="{{ url('assets/admin/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>
    <script src="{{ url('assets/admin/vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <!-- Argon JS -->
    <script src="{{ url('assets/admin/js/argon.js?v=1.1.0') }}"></script>
    <script src="{{ url('assets/admin/js/custom.js') }}"></script>

    <script>
        window.cek  = 0;

        function validasi() {
            var user    = $('#input-user').val();
            var pass    = $('#input-pass').val();

            if (user == '') {
                $('#alert-user').show();
                window.cek  = 0;
            } else {
                $('#alert-user').hide();
                window.cek  = 1;
            }

            if (pass == '') {
                $('#alert-pass').show();
                window.cek  = 0;
            } else {
                $('#alert-pass').hide();
                window.cek  = 1;
            }
        }

        $('#btnLogin').on('click', function() {
            validasi();
            var user    = $('#input-user').val();
            var pass    = $('#input-pass').val();

            if (window.cek == 1) {
                $.ajax({
                    type: "post",
                    url: "{{ url('login') }}",
                    data: {
                        user: user,
                        pass: pass
                    },
                    beforeSend: function () {
                        swal({
                            title:'Memeriksa Data',
                            text: 'Mohon Tunggu...',
                            allowOutsideClick: false,
                            onOpen: function() {
                                swal.showLoading()
                            }
                        })
                    },
                    success: function (data) {
                        swal.close();

                        if (data.msg == 'success') {
                            window.location.href = '{{ url("wp-admin") }}';
                        } else {
                            swal({
                                type: 'error',
                                title: 'Proses Masuk Gagal!',
                                text: data.msg_data,
                                allowOutsideClick: true
                            })
                        }
                    },
                    error: function (data) {
                        swal.close();

                        swal({
                            type: 'error',
                            title: 'Proses Masuk Gagal!',
                            text: data.msg_data,
                            allowOutsideClick: true
                        })
                    }
                });
            }
        })

        $('#input-user, #input-pass').keyup( function(e) {
            if (e.which == 13) {
                $('#btnLogin').click();
            }

            validasi();
        })
    </script>
</body>

</html>
