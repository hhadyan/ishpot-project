<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="javascript:void(0)">
                <img src="{{ url('assets/upload/konten/logo.png') }}" class="navbar-brand-img" alt="...">
            </a>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link {{{ (Request::is('wp-admin') ? 'active' : '') }}}" href="{{ url('/wp-admin') }}">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>

                <!-- Divider -->
                <hr class="my-3">

                <!-- Heading -->
                <h6 class="navbar-heading p-0 text-muted">
                    <span class="docs-normal">Post</span>
                </h6>

                <ul class="navbar-nav mb-md-3">
                    <li class="nav-item">
                        <a class="nav-link {{{ (Request::is('cafe') ? 'active' : (Request::is('cafe/insert') ? 'active' : 'collapsed')) }}}" href="#navbar-cafe" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-cafe">
                            <i class="ni ni-archive-2 text-primary"></i>
                            <span class="nav-link-text">Cafe</span>
                        </a>
                        <div class="collapse {{{ (Request::is('cafe') ? 'show' : (Request::is('cafe/insert') ? 'show' : '')) }}}" id="navbar-cafe" style="">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item {{{ (Request::is('cafe') ? 'active' : '') }}}">
                                    <a href="{{ url('/cafe') }}" class="nav-link">
                                        <span class="sidenav-normal"> Lihat Post </span>
                                    </a>
                                </li>
                                <li class="nav-item {{{ (Request::is('cafe/insert') ? 'active' : '') }}}">
                                    <a href="{{ url('/cafe/insert') }}" class="nav-link">
                                        <span class="sidenav-normal"> Tambah Post </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link  {{{ (Request::is('product') ? 'active' : (Request::is('product/insert') ? 'active' : 'collapsed')) }}}" href="#navbar-prod" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-prod">
                            <i class="ni ni-laptop text-primary"></i>
                            <span class="nav-link-text">Production House</span>
                        </a>
                        <div class="collapse {{{ (Request::is('product') ? 'show' : (Request::is('product/insert') ? 'show' : '')) }}}" id="navbar-prod" style="">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item {{{ (Request::is('product') ? 'active' : '') }}}">
                                    <a href="{{ url('/product') }}" class="nav-link">
                                        <span class="sidenav-normal"> Lihat Post </span>
                                    </a>
                                </li>
                                <li class="nav-item {{{ (Request::is('product/insert') ? 'active' : '') }}}">
                                    <a href="{{ url('/product/insert') }}" class="nav-link">
                                        <span class="sidenav-normal"> Tambah Post </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{{ (Request::is('esport') ? 'active' : (Request::is('esport/insert') ? 'active' : 'collapsed')) }}}" href="#navbar-esport" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-esport">
                            <i class="ni ni-controller text-primary"></i>
                            <span class="nav-link-text">E-Sport</span>
                        </a>
                        <div class="collapse {{{ (Request::is('esport') ? 'show' : (Request::is('esport/insert') ? 'show' : '')) }}}" id="navbar-esport" style="">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item {{{ (Request::is('esport') ? 'active' : '') }}}">
                                    <a href="{{ url('/esport') }}" class="nav-link">
                                        <span class="sidenav-normal"> Lihat Post </span>
                                    </a>
                                </li>
                                <li class="nav-item {{{ (Request::is('esport/insert') ? 'active' : '') }}}">
                                    <a href="{{ url('/esport/insert') }}" class="nav-link">
                                        <span class="sidenav-normal"> Tambah Post </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>

                @if (session('role') == '01' || session('role') == '02')
                    <!-- Divider -->
                    <hr class="my-3">

                    <h6 class="navbar-heading p-0 text-muted">
                        <span class="docs-normal">Akun</span>
                    </h6>

                    <ul class="navbar-nav mb-md-3">
                        <li class="nav-item">
                            <a class="nav-link {{{ (Request::is('admin') ? 'active' : (Request::is('admin/insert') ? 'active' : 'collapsed')) }}}" href="#navbar-admin" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-cafe">
                                <i class="ni ni-single-02 text-primary"></i>
                                <span class="nav-link-text">Admin</span>
                            </a>
                            <div class="collapse {{{ (Request::is('admin') ? 'show' : (Request::is('admin/insert') ? 'show' : '')) }}}" id="navbar-admin" style="">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item {{{ (Request::is('admin') ? 'active' : '') }}}">
                                        <a href="{{ url('/admin') }}" class="nav-link">
                                            <span class="sidenav-normal"> Data Admin </span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{{ (Request::is('admin/insert') ? 'active' : '') }}}">
                                        <a href="{{ url('/admin/insert') }}" class="nav-link">
                                            <span class="sidenav-normal"> Tambah Admin </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                @endif

                {{-- <!-- Heading -->
                <h6 class="navbar-heading p-0 text-muted">
                    <span class="docs-normal">Laporan Transaksi</span>
                </h6>

                <ul class="navbar-nav mb-md-3">
                    <li class="nav-item">
                        <a class="nav-link" href="examples/icons.html">
                            <i class="ni ni-planet text-orange"></i>
                            <span class="nav-link-text">Cafe</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="examples/map.html">
                            <i class="ni ni-pin-3 text-primary"></i>
                            <span class="nav-link-text">Production House</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="examples/profile.html">
                            <i class="ni ni-single-02 text-yellow"></i>
                            <span class="nav-link-text">E-Sport</span>
                        </a>
                    </li>
                </ul> --}}
            </div>

        </div>
    </div>
</nav>
