<!-- Argon Scripts -->
<!-- Core -->
<script src="{{ url('assets/admin/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ url('assets/admin/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ url('assets/admin/vendor/js-cookie/js.cookie.js') }}"></script>
<script src="{{ url('assets/admin/js/custom.js') }}"></script>
<script src="{{ url('assets/admin/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
<script src="{{ url('assets/admin/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>
<!-- Optional JS -->
<script src="{{ url('assets/admin/vendor/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ url('assets/admin/vendor/chart.js/dist/Chart.extension.js') }}"></script>
<script src="{{ url('assets/admin/vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ url('assets/admin/vendor/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
<script src="{{ url('assets/admin/vendor/bootstrap-table/bootstrap-table.min.js') }}"></script>
<script src="{{ url('assets/admin/vendor/select2/dist/js/select2.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://raw.githubusercontent.com/igorescobar/jQuery-Mask-Plugin/master/src/jquery.mask.js"></script>
<!-- Argon JS -->
<script src="{{ url('assets/admin/js/argon.js?v=1.2.0') }}"></script>
