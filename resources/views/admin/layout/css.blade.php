<!-- Favicon -->
<link rel="icon" href="{{ url('assets/admin/img/brand/favicon.png') }}" type="image/png">
<!-- Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
<!-- Icons -->
<link rel="stylesheet" href="{{ url('assets/admin/vendor/nucleo/css/nucleo.css') }}" type="text/css">
<link rel="stylesheet" href="{{ url('assets/admin/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" type="text/css">
<!-- Page plugins -->
<link rel="stylesheet" href="{{ url('assets/admin/vendor/sweetalert2/dist/sweetalert2.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ url('assets/admin/vendor/animate.css/animate.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ url('assets/admin/vendor/bootstrap-table/bootstrap-table.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ url('assets/admin/vendor/select2/dist/css/select2.min.css') }}">
<script src="https://cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!-- Argon CSS -->
<link rel="stylesheet" href="{{ url('assets/admin/css/argon.css?v=1.2.0') }}" type="text/css">
{{-- <script src="{{ url('assets/admin/vendor/ckeditor/ckeditor.js') }}"></script> --}}
