<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ishpot Indonesia | Admin</title>
        @include('admin.layout.css')
        @yield('css-raw')
    </head>

    <body>
        <!-- Sidenav -->
        @include('admin.layout.sidebar')

        <!-- Main content -->
        <div class="main-content" id="panel">
            <!-- Topnav -->
            @include('admin.layout.navbar')
            <!-- Header -->
            <div class="header bg-primary pb-6">
                <div class="container-fluid">
                    @yield('header-content')
                </div>
            </div>

            <!-- Page content -->
            <div class="container-fluid mt--6">
                <div class="row">
                    <!-- main konten -->
                    @yield('content')
                    <!-- mian konten-->
                </div>
                <!-- Footer -->
                @include('admin.layout.footer')
            </div>
        </div>
        <!-- Javascript -->
        @include('admin.layout.js')
        @yield('js-raw')
    </body>

</html>
