@extends('user.layout.layout')
@section('content')

    <div class="products-catagories-area clearfix">
        <!-- header -->
        <header id="header" class="header">
            <div class="header-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-container">
                                <h1>ISHPOT INDONESIA</h1>
                                <p class="p-large">Perusahaan Ishpot Indonesia</p>
                                <a class="btn-solid-lg page-scroll" href="#profil">DISCOVER</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header -->

        <!-- profil ishpot -->
        <div id="profil" class="basic-1">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="image-container">
                            <img class="img-fluid" src="{{ url('assets/upload/konten/ishpot-intro.jpg') }}" alt="alternative">
                        </div> <!-- end of image-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-5">
                        <div class="text-container">
                            <div class="section-title">Ishpot Indonesia</div>
                            <h2>We Offer Some Of The Best Business Growth Services In Town</h2>
                            <p>Launching a new company or developing the market position of an existing one can be quite an overwhelming processs at times.</p>
                            <p class="testimonial-text">"Our mission here at Aira is to get you through those tough moments relying on our team's expertise in starting and growing companies."</p>
                            <div class="testimonial-author">Louise Donovan - CEO</div>
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div>
        <!-- end profil ishpot -->

        <!-- detail ishpot -->
        <div id="profil" class="basic-1">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="tabs tabs-container">

                            <ul class="nav nav-tabs" id="ariaTabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="nav-tab-1" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true"><i class="fas fa-th"></i> Ishpot Cafe</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="nav-tab-2" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false"><i class="fas fa-th"></i> Production House</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="nav-tab-3" data-toggle="tab" href="#tab-3" role="tab" aria-controls="tab-3" aria-selected="false"><i class="fas fa-th"></i> E-Sport</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="ariaTabsContent">

                                <!-- Tab -->
                                <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="tab-1">
                                    <h4>Business Services For Companies</h4>
                                    <p>Aria provides the most innovative and customized business services in the industry. Our <a class="green page-scroll" href="#services">Services</a> section shows how flexible we are for all types of budgets.</p>
                                    <a class="btn-solid-lg" href="https://gofood.link/a/A8q6rEd" target="_blank">Go-Food</a>
                                </div> <!-- end of tab-pane -->
                                <!-- end of tab -->

                                <!-- Tab -->
                                <div class="tab-pane fade" id="tab-2" role="tabpanel" aria-labelledby="tab-2">
                                    <ul class="list-unstyled li-space-lg first">
                                        <li class="media">
                                            <div class="media-bullet">1</div>
                                            <div class="media-body"><strong>High quality</strong> is on top of our list when it comes to the way we deliver the services</div>
                                        </li>
                                        <li class="media">
                                            <div class="media-bullet">2</div>
                                            <div class="media-body"><strong>Maximum impact</strong> is what we look for in our actions</div>
                                        </li>
                                        <li class="media">
                                            <div class="media-bullet">3</div>
                                            <div class="media-body"><strong>Quality standards</strong> are important but meant to be exceeded</div>
                                        </li>
                                    </ul>
                                    <ul class="list-unstyled li-space-lg last">
                                        <li class="media">
                                            <div class="media-bullet">4</div>
                                            <div class="media-body"><strong>We're always looking</strong> for industry leaders to help them win their market position</div>
                                        </li>
                                        <li class="media">
                                            <div class="media-bullet">5</div>
                                            <div class="media-body"><strong>Evaluation</strong> is a key aspect of this business and important</div>
                                        </li>
                                        <li class="media">
                                            <div class="media-bullet">6</div>
                                            <div class="media-body"><strong>Ethic</strong> procedures ar alwasy at the base of everything we do</div>
                                        </li>
                                    </ul>
                                </div> <!-- end of tab-pane -->
                                <!-- end of tab -->

                                <!-- Tab -->
                                <div class="tab-pane fade" id="tab-3" role="tabpanel" aria-labelledby="tab-3">
                                    <p><strong>We strive to achieve</strong> 100% customer satisfaction for both types of customers: hiring companies and job seekers. Types of customers: <a class="green" href="#your-link">with huge potential</a></p>
                                    <p><strong>Our goal is to help</strong> your company achieve its full potential and establish long term stability for <a class="green" href="#your-link">the stakeholders</a></p>
                                    <ul class="list-unstyled li-space-lg">
                                        <li class="media">
                                            <i class="fas fa-square"></i>
                                            <div class="media-body">It's easy to get a quotation, just call our office anytime</div>
                                        </li>
                                        <li class="media">
                                            <i class="fas fa-square"></i>
                                            <div class="media-body">We'll get back to you with an initial estimate soon</div>
                                        </li>
                                        <li class="media">
                                            <i class="fas fa-square"></i>
                                            <div class="media-body">Get ready to see results even after only 30 days</div>
                                        </li>
                                        <li class="media">
                                            <i class="fas fa-square"></i>
                                            <div class="media-body">Ask for a quote and start improving your business</div>
                                        </li>
                                        <li class="media">
                                            <i class="fas fa-square"></i>
                                            <div class="media-body">Just fill out the form and we'll call you right away</div>
                                        </li>
                                    </ul>
                                </div> <!-- end of tab-pane -->
                                <!-- end of tab -->

                            </div>

                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="image-container">
                            <img class="img-fluid" src="{{ url('assets/upload/konten/ishpot-intro.jpg') }}" alt="alternative">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end detail ishpot -->

        <!-- fokus banyumas -->
        <div id="fokusbanyumas" class="basic-1">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="image-container">
                            <img class="img-fluid" src="{{ url('assets/upload/konten/fokba-intro.jpg') }}" alt="alternative">
                        </div> <!-- end of image-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-5">
                        <div class="text-container">
                            <div class="section-title">Fokus Banyumas</div>
                            <h2>We Offer Some Of The Best Business Growth Services In Town</h2>
                            <p>Launching a new company or developing the market position of an existing one can be quite an overwhelming processs at times.</p>
                            <p class="testimonial-text">"Our mission here at Aira is to get you through those tough moments relying on our team's expertise in starting and growing companies."</p>
                            <div class="testimonial-author">Louise Donovan - CEO</div>
                            <a class="btn-solid-lg" href="https://fokusbanyumas.id/" target="_blank">Kunjungi Laman</a>
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div>
        <!-- end fokus banyumas -->
    </div>
@endsection
