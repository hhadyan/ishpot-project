<!-- Mobile Nav (max width 767px)-->
<div class="mobile-nav">
    <!-- Navbar Brand -->
    <div class="amado-navbar-brand">
        <a href="/"><img src="{{ url('assets/upload/konten/logo.png') }}" alt=""></a>
    </div>
    <!-- Navbar Toggler -->
    <div class="amado-navbar-toggler">
        <span></span><span></span><span></span>
    </div>
</div>

<!-- Header Area Start -->
<header class="header-area clearfix">
    <!-- Close Icon -->
    <div class="nav-close">
        <i class="fa fa-close" aria-hidden="true"></i>
    </div>
    <!-- Logo -->
    <div class="logo">
        <a href="/"><img src="{{ url('assets/upload/konten/logo.png') }}" alt=""></a>
    </div>
    <!-- Amado Nav -->
    <nav class="amado-nav">
        <ul>
            <li {{{ (Request::is('/') ? 'class=active' : '') }}}><a href="/">Home</a></li>
            <li><a href="{{{ (Request::is('/') ? '#profil' : '/') }}}">Tentang Kami</a></li>
            <li {{{ (Request::is('menu-cafe') ? 'class=active' : '') }}}><a href="{{ url('/menu-cafe') }}">Ishpot Cafe</a></li>
            <li {{{ (Request::is('production-house') ? 'class=active' : '') }}}><a href="{{ url('/production-house') }}">Production House</a></li>
            <li {{{ (Request::is('e-sport') ? 'class=active' : '') }}}><a href="{{ url('/e-sport') }}">e-Sport</a></li>
            <li><a href="{{{ (Request::is('/') ? '#fokusbanyumas' : '/') }}}">FokusBanyumas</a></li>
            <br>
            <br>
            <br>
            {{-- <li {{{ (Request::is('cart') ? 'class=active' : '') }}}><a href="{{ url('/cart') }}" class="cart-nav"><img src="{{ url('assets/user/img/core-img/cart.png') }}" alt=""> Keranjang <span>(0)</span></a></li>
            <li><a href="#" class="fav-nav"><img src="{{ url('assets/user/img/core-img/favorites.png') }}" alt=""> Favourite</a></li> --}}
        </ul>
    </nav>
    <!-- Button Group -->
    <div class="amado-btn-group mt-30 mb-100">
    </div>
    <!-- Social Button -->
    <div class="social-info d-flex justify-content-between">
        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
    </div>
</header>
<!-- Header Area End -->
