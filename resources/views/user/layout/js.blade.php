<!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
<script src="{{ url('assets/user/js/jquery/jquery-2.2.4.min.js') }}"></script>
<!-- Popper js -->
<script src="{{ url('assets/user/js/popper.min.js') }}"></script>
<!-- Bootstrap js -->
<script src="{{ url('assets/user/js/bootstrap.min.js') }}"></script>
<!-- Plugins js -->
<script src="{{ url('assets/user/js/plugins.js') }}"></script>
<!-- Active js -->
<script src="{{ url('assets/user/js/active.js') }}"></script>
<script src="{{ url('assets/admin/js/custom.js') }}"></script>
