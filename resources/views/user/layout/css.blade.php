<!-- Favicon  -->
<link rel="icon" href="{{ url('assets/user/img/core-img/favicon.ico') }}">

<!-- Core Style CSS -->
<link rel="stylesheet" href="{{ url('assets/user/css/core-style.css') }}">
<link rel="stylesheet" href="{{ url('assets/user/css/style.css') }}">
<link rel="stylesheet" href="{{ url('assets/user/css/fontawesome-all.css') }}">

@yield('stylecss')
