@extends('user.layout.layout')
@section('content')

    <div class="shop_sidebar_area">

        <!-- ##### Single Widget ##### -->
        <div class="widget catagory mb-50">
            <!-- Widget Title -->
            <h6 class="widget-title mb-30">Catagories</h6>
            <!--  Catagories  -->
            <div class="catagories-menu">
                <ul>
                    <li onclick="getData(0)"><a href="#">Semua Menu</a></li>
                    @foreach ($kategori as $val)
                        <li onclick="getData({{ $val->id }})"><a href="#">{{ $val->kategori }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="amado_product_area section-padding-100">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="product-topbar d-xl-flex align-items-end justify-content-between">
                        <!-- Total Products -->
                        <div class="total-products">
                            <p>Showing 1-8 0f 25</p>
                        </div>
                        <!-- Sorting -->
                        <div class="product-sorting d-flex">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="katalog">

            </div>

            <div class="row">
                <div class="col-12">
                    <!-- Pagination -->
                    <nav aria-label="navigation">
                        <ul class="pagination justify-content-end mt-50">
                            <li class="page-item active"><a class="page-link" href="#">01.</a></li>
                            <li class="page-item"><a class="page-link" href="#">02.</a></li>
                            <li class="page-item"><a class="page-link" href="#">03.</a></li>
                            <li class="page-item"><a class="page-link" href="#">04.</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('customjquery')
    <script>
        $(document).ready(function () {
            getData(0);
        });

        function getData(kategori) {
            $.ajax({
                type: "POST",
                url: "{{ url('/menu-cafe/get') }}",
                data: {
                    kategori:kategori
                },
                success: function (data) {
                    konten  = '';

                    for (let x = 0; x < data.length; x++) {
                        img     = '{{ url("assets/upload/menu") }}' + '/' + data[x].foto;
                        konten +=   '<div class="col-12 col-sm-6 col-md-12 col-xl-6">' +
                                        '<div class="single-product-wrapper">' +
                                            '<div class="product-img">' +
                                                '<img src="' + img + '" alt="">' +
                                            '</div>' +
                                            '<div class="product-description d-flex align-items-center justify-content-between">' +
                                                '<div class="product-meta-data">' +
                                                    '<div class="line"></div>' +
                                                    '<p class="product-price">' + moneyFormatter(data[x].harga) + '</p>' +
                                                    '<a href="product-details.html">' +
                                                        '<h6>' + data[x].nama + '</h6>' +
                                                    '</a>' +
                                                '</div>' +
                                                '<div class="ratings-cart text-right">' +
                                                    '<div class="cart">' +
                                                        '<a href="cart.html" data-toggle="tooltip" data-placement="left" title="Add to Cart"><img src="assets/user/img/core-img/cart.png" alt=""></a>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>';

                    }

                    $('#katalog').html(konten);
                }
            });
        }
    </script>
@endsection
