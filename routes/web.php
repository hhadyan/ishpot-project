<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//route User
Route::get('/', 'User\HomeController@index');
    //route menu user
        //cafe
        Route::get('/menu-cafe', 'User\CafeController@index');
        Route::post('/menu-cafe/get', 'User\CafeController@getData');
        //production house
        Route::get('/production-house', 'User\ProductionController@index');
        Route::post('/production-house/get', 'User\ProductionController@getData');
        //e-sport
        Route::get('/e-sport', 'User\EsportController@index');
        Route::post('/e-sport/get', 'User\EsportController@getData');
        //cart
        Route::get('/cart', 'User\CartController@index');

//route Admin
Route::get('/wp-admin', 'Admin\HomeController@index');
    //route API admin
    Route::post('/publish', 'Admin\APIController@publish');
    Route::post('/publish-all', 'Admin\APIController@publishAll');
    Route::post('/draft', 'Admin\APIController@draft');
    Route::post('/draft-all', 'Admin\APIController@draftAll');
    Route::post('/delete', 'Admin\APIController@delete');

    Route::get('/login', 'Admin\AuthController@index');
    Route::post('/login', 'Admin\AuthController@login');
    Route::get('/logout', 'Admin\AuthController@logout');

    //route menu admin
        //cafe
        Route::get('/cafe', 'Admin\CafeController@index');
        Route::get('/cafe/insert', 'Admin\CafeController@input');
        Route::get('/cafe/edit/{id}', 'Admin\CafeController@edit');
        Route::post('/cafe/publish', 'Admin\CafeController@publish');
        Route::post('/cafe/draft', 'Admin\CafeController@draft');
        Route::post('/cafe/update', 'Admin\CafeController@update');
        Route::post('/cafe/delete', 'Admin\CafeController@delete');
        Route::post('/cafe/get', 'Admin\CafeController@ambilData');

        //product
        Route::get('/product', 'Admin\ProductController@index');
        Route::get('/product/insert', 'Admin\ProductController@input');
        Route::get('/product/edit/{id}', 'Admin\ProductController@edit');
        Route::post('/product/publish', 'Admin\ProductController@publish');
        Route::post('/product/draft', 'Admin\ProductController@draft');
        Route::post('/product/update', 'Admin\ProductController@update');
        Route::post('/product/delete', 'Admin\ProductController@delete');
        Route::post('/product/get', 'Admin\ProductController@ambilData');

        //E-sport
        Route::get('/esport', 'Admin\EsportController@index');
        Route::get('/esport/insert', 'Admin\EsportController@input');
        Route::get('/esport/edit/{id}', 'Admin\EsportController@edit');
        Route::post('/esport/publish', 'Admin\EsportController@publish');
        Route::post('/esport/draft', 'Admin\EsportController@draft');
        Route::post('/esport/update', 'Admin\EsportController@update');
        Route::post('/esport/delete', 'Admin\EsportController@delete');
        Route::post('/esport/get', 'Admin\EsportController@ambilData');

        //Admin Account
        Route::get('/admin', 'Admin\AdminController@index');
        Route::get('/admin/insert', 'Admin\AdminController@input');
        Route::get('/admin/edit/{id}', 'Admin\AdminController@edit');
        Route::post('/admin/save', 'Admin\AdminController@save');
        Route::post('/admin/update', 'Admin\AdminController@update');
        Route::post('/admin/delete', 'Admin\AdminController@delete');
        Route::post('/admin/get', 'Admin\AdminController@ambilData');
