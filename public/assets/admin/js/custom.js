$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function APIsimpan(link, formdata) {
    $.ajax({
        type: "POST",
        url: link,
        data: formdata,
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                title:'Menyimpan Data',
                text: 'Mohon Tunggu...',
                allowOutsideClick: false,
                onOpen: function() {
                    swal.showLoading()
                }
            })
        },
        success: function (data) {
            swal.close();

            if (data.msg == 'success') {
                swal({
                    type:'success',
                    title:'Berhasil Menyimpan Data',
                    allowOutsideClick: true
                }).then(function() {
                    location.reload();
                })
            } else {
                swal({
                    type: 'error',
                    title: 'Gagal Menyimpan Data',
                    text: data.msg_data,
                    allowOutsideClick: true
                })
            }
        },
        error: function (data) {
            swal.close();

            swal({
               type: 'error',
               title: 'Gagal Menyimpan Data',
               text: data.msg_data,
               allowOutsideClick: true
            })
        }
    });
}

function APIupdate(link, formdata) {
    $.ajax({
        type: "POST",
        url: link,
        data: formdata,
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                title:'Merubah Data',
                text: 'Mohon Tunggu...',
                allowOutsideClick: false,
                onOpen: function() {
                    swal.showLoading()
                }
            })
        },
        success: function (data) {
            swal.close();
            if (data.msg == 'success') {
                swal({
                    type:'success',
                    title:'Berhasil Merubah Data',
                    allowOutsideClick: true
                }).then(function() {
                    location.reload();
                })
            } else {
                swal({
                    type: 'error',
                    title: 'Gagal Merubah Data',
                    text: data.msg_data,
                    allowOutsideClick: true
                })
            }
        },
        error: function (data) {
            swal.close();

            swal({
               type: 'error',
               title: 'Gagal Merubah Data',
               text: data.msg_data,
               allowOutsideClick: true
            })
        }
    });
}

function APIshowAll(link, tabel) {
    window.no = 0;
    $.ajax({
        type: "GET",
        url: link,
        success: function (data) {
            $(tabel).bootstrapTable('load', data);
        },
        error: function (data) {
            swal({
               type: 'error',
               title: 'Gagal Mengambil Data',
               text: data.msg_data,
               allowOutsideClick: true
            })
        }
    });
}

function APIshowFiltered(link, tabel, data, button) {
    window.no = 0;
    $.ajax({
        type: "POST",
        url: link,
        data: data,
        beforeSend: function () {
            if (button == '1') {
                swal({
                    title:'Mencari Data',
                    text: 'Mohon Tunggu...',
                    allowOutsideClick: false,
                    onOpen: function() {
                        swal.showLoading()
                    }
                })
            }
        },
        success: function (data) {
            if (button == '1') {
                swal.close();
                if (data) {
                    swal({
                        type:'success',
                        title:'Berhasil Mencari Data',
                        allowOutsideClick: true
                    })
                } else {
                    swal({
                        type: 'error',
                        title: 'Gagal Mencari Data',
                        text: data.msg_data,
                        allowOutsideClick: true
                    })
                }
            }

            $(tabel).bootstrapTable('load', data);
        },
        error: function (data) {
            swal({
               type: 'error',
               title: 'Gagal Mengambil Data',
               text: data.msg_data,
               allowOutsideClick: true
            })
        }
    });
}

function APIpublish(tabel, id, callback) {
    $.ajax({
        type: "POST",
        url: "/publish",
        data: {
            tabel: tabel,
            id: id
        },
        beforeSend: function () {
            swal({
                title:'Merubah Status',
                text: 'Mohon Tunggu...',
                allowOutsideClick: false,
                onOpen: function() {
                    swal.showLoading()
                }
            })
        },
        success: function (data) {
            swal.close();

            if (data.msg == 'success') {
                swal({
                    type:'success',
                    title:'Berhasil Merubah Status',
                    allowOutsideClick: true
                })

                callback();
            } else {
                swal({
                    type: 'error',
                    title: 'Gagal Menyimpan Data',
                    text: data.msg_data,
                    allowOutsideClick: true
                })
            }
        },
        error: function (data) {
            swal.close();

            swal({
               type: 'error',
               title: 'Gagal Menyimpan Data',
               text: data.msg_data,
               allowOutsideClick: true
            })
        }
    });
}

function APIdraft(tabel, id, callback) {
    $.ajax({
        type: "POST",
        url: "/draft",
        data: {
            tabel: tabel,
            id: id
        },
        beforeSend: function () {
            swal({
                title:'Merubah Status',
                text: 'Mohon Tunggu...',
                allowOutsideClick: false,
                onOpen: function() {
                    swal.showLoading()
                }
            })
        },
        success: function (data) {
            swal.close();

            if (data.msg == 'success') {
                swal({
                    type:'success',
                    title:'Berhasil Merubah Status',
                    allowOutsideClick: true
                })

                callback();
            } else {
                swal({
                    type: 'error',
                    title: 'Gagal Menyimpan Data',
                    text: data.msg_data,
                    allowOutsideClick: true
                })
            }
        },
        error: function (data) {
            swal.close();

            swal({
               type: 'error',
               title: 'Gagal Menyimpan Data',
               text: data.msg_data,
               allowOutsideClick: true
            })
        }
    });
}

function APIdelete(tabel, id, callback) {
    swal({
        title: "Anda Yakin untuk Menghapus Data Ini?",
        text: "Data yang telah dihapus tidak dapat dikembalikan!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText:'Hapus',
        confirmButtonColor: '#d33',
        cancelButtonText:'Kembali'
    }).then((respon) => {
        if (respon.value == true) {
            $.ajax({
                type: "POST",
                url: "/delete",
                data: {
                    tabel: tabel,
                    id: id
                },
                beforeSend: function () {
                    swal({
                        title:'Menghapus Data',
                        text: 'Mohon Tunggu...',
                        allowOutsideClick: false,
                        onOpen: function() {
                            swal.showLoading()
                        }
                    })
                },
                success: function (data) {
                    swal.close();

                    if (data.msg == 'success') {
                        swal({
                            type:'success',
                            title:'Berhasil Menghapus Data',
                            allowOutsideClick: true
                        })

                        callback();
                    } else {
                        swal({
                            type: 'error',
                            title: 'Gagal Menghapus Data',
                            text: data.msg_data,
                            allowOutsideClick: true
                        })
                    }

                },
                error: function (data) {
                    swal.close();

                    swal({
                        type: 'error',
                        title: 'Gagal Menghapus Data',
                        text: data.msg_data,
                        allowOutsideClick: true
                    })
                }
            });
        }
    })
}

function localeDate(date) {
    var d       = new Date(date);

    var date    = d.getDate();
    var month   = d.getMonth();
    var year    = d.getFullYear();

    return date + '-' + month + '-' + year;
}

function numberFormatter() {
    window.no += 1;
    return window.no;
}

function moneyFormatter(value) {
    var count   = value.toString().length;
    if (count >= 3) {
        return "Rp " + (value/1000).toFixed(3);
    } else {
        return "Rp " + value;
    }
}
